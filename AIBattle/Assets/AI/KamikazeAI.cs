﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor.Experimental.UIElements.GraphView;
using UnityEngine;

[CreateAssetMenu(fileName = "KamikazeAI", menuName = "AI/Kamikaze AI")]
public class KamikazeAI : BaseAI
{
    class Node
    {
        public Dictionary<GameManager.Direction, Node> connections = new Dictionary<GameManager.Direction, Node>();
        public GameManager.SensorData type;
        public Vector2 coordinates = new Vector2();
        public Node parentNode;
        public Node()
        {
            connections.Add(GameManager.Direction.Up, null);
            connections.Add(GameManager.Direction.Down, null);
            connections.Add(GameManager.Direction.Left, null);
            connections.Add(GameManager.Direction.Right, null);
        }
    }
    Dictionary<Vector2, Node> map = new Dictionary<Vector2, Node>();
    Dictionary<Vector2, Node> frontier = new Dictionary<Vector2, Node>();
    int bombing = 0;
    Node goal = new Node(); Node keyPos = new Node(); bool hasKey = false; bool foundGoal = false; bool foundKey = false; bool foundEnemy = false;
    Vector2 currentPos = new Vector2(0, 0); bool path = false; Dictionary<Vector2, Node> currentPath = new Dictionary<Vector2, Node>();
    bool kill = false; GameManager.Direction killDir = GameManager.Direction.Up;
    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime)
    {
        Scan();
        bool move = false; GameManager.Direction direction = (GameManager.Direction)UnityEngine.Random.Range(0, 4);
        frontier.Remove(currentPos); //Debug.Log(frontier.Count);
        ConnectTiles();
        if (kill)
        {
            if (killDir == GameManager.Direction.Current)
            {
                aBombTime = 2; bombing = 2;
                return CombatantAction.DropBomb;
            }
        }
        if (foundEnemy && bombing<=0)
        {
            aBombTime = 2; bombing = 2;
            return CombatantAction.DropBomb;
        }
        if (foundGoal && !foundKey && bombing <= 0)
        {
            if (currentPos == goal.coordinates)
            {
                aBombTime = 3; bombing = 4;
                return CombatantAction.DropBomb;
            }
            else
            {
                currentPath = BreadthFirstSearch(map[currentPos], goal);
                path = true;
            }
        }
        bombing--;
        if (foundKey && keyPos.coordinates == currentPos)
        {
            hasKey = true; //Debug.Log("Got key!");
            if (foundGoal)
            {
                //Debug.Log("Stepping to goal!");
                currentPath = BreadthFirstSearch(map[currentPos], goal);
                path = true;
            }
        }
        if (foundGoal && hasKey)
        {
            //Debug.Log("Going to goal!");
            currentPath = BreadthFirstSearch(map[currentPos], goal);
            path = true;
        }
        foreach (Node n in map[currentPos].connections.Values)
        {
            if (n != null)
                if (foundKey && n.coordinates == keyPos.coordinates && !hasKey)
                {
                    //Debug.Log("Going to key!");
                    currentPath = BreadthFirstSearch(map[currentPos], keyPos);
                    path = true;
                }
        }
        while (!move)
        {
            if (!path)
            {
                if (!move && !path)
                {
                    currentPath = BreadthFirstSearch(map[currentPos], frontier.Last().Value);
                    path = true;
                }
                if (UseSensor(direction) != GameManager.SensorData.Wall && UseSensor(direction) != GameManager.SensorData.OffGrid)
                    move = true;
                else direction = (GameManager.Direction)UnityEngine.Random.Range(0, 4);
            }
            if (path)
            {
                Vector2 pathTracker = currentPos;
                for (int i = 0; i < currentPath.Count; i++)
                {
                    Vector2 diff = pathTracker - currentPath.Last().Key;
                    if (diff.x == 0 & diff.y == 0)
                    {
                        currentPath.Remove(currentPath.Last().Key);
                        diff = pathTracker - currentPath.Last().Key;
                    }
                    diff = diff * -1;
                    string printout = "Moving";
                    foreach (Vector2 v in currentPath.Keys)
                    {
                        printout += " to " + v.ToString();
                    }
                    //Debug.Log(printout);
                    if (diff.x == 1 && diff.y == 0)
                    { aMoves.Add(GameManager.Direction.Right); AddDirection(GameManager.Direction.Right); pathTracker.x += 1; currentPath.Remove(currentPath.Last().Key); }
                    else if (diff.x == 0 && diff.y == -1)
                    { aMoves.Add(GameManager.Direction.Down); AddDirection(GameManager.Direction.Down); pathTracker.y -= 1; currentPath.Remove(currentPath.Last().Key); }
                    else if (diff.x == -1 && diff.y == 0)
                    { aMoves.Add(GameManager.Direction.Left); AddDirection(GameManager.Direction.Left); pathTracker.x -= 1; currentPath.Remove(currentPath.Last().Key); }
                    else if (diff.x == 0 && diff.y == 1)
                    { aMoves.Add(GameManager.Direction.Up); AddDirection(GameManager.Direction.Up); pathTracker.y += 1; currentPath.Remove(currentPath.Last().Key); }
                    else
                    {
                        path = false; currentPath.Clear();
                    }
                }
                path = false; return CombatantAction.Move;
            }
        }
        return CombatantAction.Pass;
    }
    void Scan()
    {
        ScanTile(currentPos, GameManager.Direction.Current);
        ScanTile(new Vector2(currentPos[0], currentPos[1] + 1), GameManager.Direction.Up);
        ScanTile(new Vector2(currentPos[0], currentPos[1] - 1), GameManager.Direction.Down);
        ScanTile(new Vector2(currentPos[0] - 1, currentPos[1]), GameManager.Direction.Left);
        ScanTile(new Vector2(currentPos[0] + 1, currentPos[1]), GameManager.Direction.Right);
        //Debug.Log(map.Count);
    }
    void ScanTile(Vector2 coordinates, GameManager.Direction dir)
    {
        bool tisNew = true;
        foreach (Vector2 v in map.Keys)
        {
            if (v.x == coordinates.x && v.y == coordinates.y)
            {
                tisNew = false; break;
            }
        }
        if (!tisNew)
        {
            map[coordinates].type = UseSensor(dir);
        }
        else
        {
            Node n = new Node();
            n.coordinates = coordinates;
            n.type = UseSensor(dir);
            map[coordinates] = n;
            if (map[coordinates].type != GameManager.SensorData.Wall && map[coordinates].type != GameManager.SensorData.OffGrid)
                frontier[coordinates] = n;
        }
        if ((GameManager.SensorData.Goal & map[coordinates].type) != 0)
        {
            foundGoal = true; goal = map[coordinates]; //Debug.Log("Found goal at " + coordinates);
        }
        if ((GameManager.SensorData.Diamond & map[coordinates].type) != 0)
        {
            if ((GameManager.SensorData.Enemy & map[coordinates].type) != 0 && coordinates == currentPos)
            {
                kill = true; killDir = dir;
            }
            else kill = false;
            foundKey = true; keyPos = map[coordinates]; //Debug.Log("Found key at " + coordinates);
        }
        if ((GameManager.SensorData.Enemy & map[coordinates].type) != 0)
        {
            foundEnemy = true; //Debug.Log("Found enemy at " + coordinates);
        }
        //Debug.Log("Scanned " + coordinates + ": " + map[coordinates].type);

    }

    void ConnectTiles()
    {
        foreach (Vector2 v in map.Keys)
        {
            ConnectPair(v, new Vector2(v.x, v.y + 1), GameManager.Direction.Up);
            ConnectPair(v, new Vector2(v.x, v.y - 1), GameManager.Direction.Down);
            ConnectPair(v, new Vector2(v.x - 1, v.y), GameManager.Direction.Left);
            ConnectPair(v, new Vector2(v.x + 1, v.y), GameManager.Direction.Right);
        }
    }

    void ConnectPair(Vector2 v1, Vector2 v2, GameManager.Direction direction)
    {
        if (map.Keys.Contains(v1))
        {
            if (map[v1].connections[direction] != null)
                return;
            else if (map.Keys.Contains(v2))
            {
                if (map[v1].type != GameManager.SensorData.Wall && map[v1].type != GameManager.SensorData.OffGrid)
                    if (map[v2].type != GameManager.SensorData.Wall && map[v2].type != GameManager.SensorData.OffGrid)
                    {
                        GameManager.Direction opposite;
                        switch (direction)
                        {
                            case GameManager.Direction.Up:
                                opposite = GameManager.Direction.Down;
                                break;
                            case GameManager.Direction.Down:
                                opposite = GameManager.Direction.Up;
                                break;
                            case GameManager.Direction.Left:
                                opposite = GameManager.Direction.Right;
                                break;
                            case GameManager.Direction.Right:
                                opposite = GameManager.Direction.Left;
                                break;
                            default:
                                opposite = GameManager.Direction.Current;
                                break;
                        }
                        map[v1].connections[direction] = map[v2];
                        map[v2].connections[opposite] = map[v1];
                        //Debug.Log("Connected " + v1.x + "," + v1.y + " and " + map[v1].connections[direction].coordinates.x + "," + map[v1].connections[direction].coordinates.y + "(" + map[v1].type + "&" + map[v1].connections[direction].type + ")");
                    }
            }
        }
        return;
    }
    Dictionary<Vector2, Node> BreadthFirstSearch(Node start, Node end)
    {
        Node youAreHere;
        List<Node> frontier = new List<Node>(); Dictionary<Vector2, Node> finalPath = new Dictionary<Vector2, Node>(); List<Node> visited = new List<Node>();
        int distanceFromStart = map.Count + 1;
        frontier.Add(start);
        Node endNode = null;
        while (frontier.Count > 0)
        {
            Node tempP = frontier.First();
            frontier.Remove(tempP);
            foreach (Node n in map.Values)
            {
                if (n != null && n.coordinates == tempP.coordinates)
                {
                    youAreHere = n;
                    if (!visited.Contains(youAreHere)) visited.Add(youAreHere);
                    foreach (Node nod in youAreHere.connections.Values)
                    {
                        if (nod != null && !visited.Contains(nod))
                        {
                            nod.parentNode = youAreHere;
                            distanceFromStart++;
                            frontier.Add(nod);
                            visited.Add(nod);
                            if (nod.coordinates == end.coordinates)
                            {
                                frontier.Clear();
                                endNode = nod;
                                break;
                            }
                        }
                    }
                }

            }
        }
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode.coordinates, endNode);
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode.coordinates, endNode);
        string printout = "Breadth-first in " + visited.Count + " steps, found a " + finalPath.Count + "-length path.";
        //foreach(PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        //Debug.Log(printout);
        return finalPath;
    }

    void AddDirection(GameManager.Direction dir)
    {
        switch (dir)
        {
            case GameManager.Direction.Up:
                currentPos.y++;
                break;
            case GameManager.Direction.Down:
                currentPos.y--;
                break;
            case GameManager.Direction.Left:
                currentPos.x--;
                break;
            case GameManager.Direction.Right:
                currentPos.x++;
                break;
        }
    }
}