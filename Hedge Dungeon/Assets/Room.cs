using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UI;

public class Room : MonoBehaviour
{
    public int[] dimensions = new int[2];
    public int doors;
    public Room[] adjacentRooms;
    public List<Room> connectedRooms;
    public Room previousRoom;
    public bool hasDoors, rightSide, topSide;
    [SerializeField] GameObject gridTile;
    public int seed; 
    public enum RoomType
    { 
    Entrance,
    Basic,
    Loot,
    Locked,
    Exit,
    Corridor,
    }
    public RoomType type;
    public Tile[,] layout;
    // Start is called before the first frame update
    void Awake()
    {
        adjacentRooms = new Room[] { null, null, null, null };
        connectedRooms = new List<Room>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateRoom()
    {
        bool needsEntrance = false; bool needsExit = false; bool needsChest = false;
        if (type == RoomType.Entrance) needsEntrance = true;
        if (type == RoomType.Exit) needsExit = true;
        if (type == RoomType.Loot || type == RoomType.Locked) needsChest = true;
        if (dimensions[0] < 3) dimensions[0] = 3;
        if (dimensions[1] < 3) dimensions[1] = 3;
        int[] stairsCoord = new int[] {0,0};
        layout = new Tile[dimensions[0], dimensions[1]];
        UnityEngine.Random.seed = seed;
        if (needsEntrance || needsExit)
        {
            stairsCoord = new int[] {UnityEngine.Random.Range(1, dimensions[0]-1), UnityEngine.Random.Range(1, dimensions[1]-1)};
        }
        if (needsChest)
        {
            stairsCoord = new int[] { UnityEngine.Random.Range(1, dimensions[0] - 1), UnityEngine.Random.Range(1, dimensions[1] - 1) };
        }
        for (int i = 0; i < dimensions[0]; i++)
            for (int j = 0; j < dimensions[1]; j++)
            {
                layout[i, j] = new Tile();
                    if (i == 0 || i == dimensions[0] - 1)
                    {
                        if (j == dimensions[1] / 2 && i == 0 && connectedRooms.Contains(adjacentRooms[1]))
                            if (hasDoors)
                                if (type == RoomType.Locked || adjacentRooms[1].type == RoomType.Locked) layout[i, j].type = Tile.TileType.LockedDoor;
                                else layout[i, j].type = Tile.TileType.Door;
                            else layout[i, j].type = Tile.TileType.Empty;
                        else if (j == dimensions[1] / 2 && i == dimensions[0] - 1 && connectedRooms.Contains(adjacentRooms[3]))
                            if (hasDoors)
                                if (type == RoomType.Locked || adjacentRooms[3].type == RoomType.Locked) layout[i, j].type = Tile.TileType.LockedDoor;
                                else layout[i, j].type = Tile.TileType.Door;
                            else layout[i, j].type = Tile.TileType.Empty;
                        else
                        {
                            if (adjacentRooms[3] != null && i == dimensions[0] - 1 && !rightSide) layout[i, j].type = Tile.TileType.Empty;
                            else layout[i, j].type = Tile.TileType.Wall;

                        }
                    }
                    else if (j == 0 || j == dimensions[1] - 1)
                    {
                        if (i == dimensions[0] / 2 && j == 0 && connectedRooms.Contains(adjacentRooms[0]))
                            if (hasDoors)
                                if (type == RoomType.Locked || adjacentRooms[0].type == RoomType.Locked) layout[i, j].type = Tile.TileType.LockedDoor;
                                else layout[i, j].type = Tile.TileType.Door;
                            else layout[i, j].type = Tile.TileType.Empty;
                        else if (i == dimensions[0] / 2 && j == dimensions[1] - 1 && connectedRooms.Contains(adjacentRooms[2]))
                            if (hasDoors)
                                if (type == RoomType.Locked || adjacentRooms[2].type == RoomType.Locked) layout[i, j].type = Tile.TileType.LockedDoor;
                                else layout[i, j].type = Tile.TileType.Door;
                            else layout[i, j].type = Tile.TileType.Empty;
                        else
                        {
                            if (adjacentRooms[0] != null && j == 0 && !topSide) layout[i, j].type = Tile.TileType.Empty;
                            else
                                layout[i, j].type = Tile.TileType.Wall;

                        }
                    }
                    else if (i == stairsCoord[0] && j == stairsCoord[1])
                    {
                    if (needsEntrance)
                    {
                        layout[i, j].type = Tile.TileType.Entrance;
                    }
                        if (needsExit)
                        {
                            layout[i, j].type = Tile.TileType.Exit;
                        }
                    if (needsChest) layout[i, j].type = Tile.TileType.Chest;
                    }
                    else if(type != RoomType.Corridor) layout[i, j].type = Tile.TileType.Floor;
                if (type == RoomType.Corridor)
                {
                    if (i == dimensions[0] / 2 && j == dimensions[1] / 2)
                        layout[i, j].type = Tile.TileType.Floor;
                    else if (i > 0 && i < dimensions[0] - 1 && j > 0 && j < dimensions[1] - 1)
                    {
                        layout[i, j].type = Tile.TileType.Wall;
                        if (connectedRooms.Contains(adjacentRooms[0]))
                        {
                            if (j <= dimensions[1] / 2 - 1 && j != 0)
                                if (i == dimensions[0] / 2 - 1 || i == dimensions[0] / 2 + 1)
                                    layout[i, j].type = Tile.TileType.Wall;
                                else if (i == dimensions[0] / 2)
                                    layout[i, j].type = Tile.TileType.Floor;
                        }
                        else if(i==dimensions[0]/2&&j==dimensions[1]/2-1)
                            layout[i, j].type = Tile.TileType.Wall;
                        if (connectedRooms.Contains(adjacentRooms[2]))
                        {
                            if (j >= dimensions[1] / 2 + 1 && j != dimensions[1] - 1)
                                if (i == dimensions[0] / 2 - 1 || i == dimensions[0] / 2 + 1)
                                    layout[i, j].type = Tile.TileType.Wall;
                                else if (i == dimensions[0] / 2)
                                    layout[i, j].type = Tile.TileType.Floor;
                        }
                        else if (i == dimensions[0] / 2 && j == dimensions[1] / 2 + 1)
                            layout[i, j].type = Tile.TileType.Wall;
                        if (connectedRooms.Contains(adjacentRooms[1]))
                        {
                            if (i <= dimensions[0] / 2 - 1 && i != 0)
                                if (j == dimensions[1] / 2 - 1 || j == dimensions[1] / 2 + 1)
                                    layout[i, j].type = Tile.TileType.Wall;
                                else if (j == dimensions[1] / 2)
                                    layout[i, j].type = Tile.TileType.Floor;
                        }
                        else if (j == dimensions[1] / 2 && i == dimensions[0] / 2 - 1)
                            layout[i, j].type = Tile.TileType.Wall;
                        if (connectedRooms.Contains(adjacentRooms[3]))
                        {
                            if (i >= dimensions[0] / 2 + 1 && i != dimensions[0] - 1)
                                if (j == dimensions[1] / 2 - 1 || j == dimensions[1] / 2 + 1)
                                    layout[i, j].type = Tile.TileType.Wall;
                                else if (j == dimensions[1] / 2)
                                    layout[i, j].type = Tile.TileType.Floor;
                        }
                        else if (j == dimensions[1] / 2 && i == dimensions[0] / 2 + 1)
                            layout[i, j].type = Tile.TileType.Wall;
                    }
                    
                }
            }
    }
}
