using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool highlighted = false;
    public bool selected = false;
    [SerializeField] List<Sprite>[] sprites = new List<Sprite>[3];
    [SerializeField] List<Sprite> FirstSprites = new List<Sprite>();
    [SerializeField] List<Sprite> SecondSprites = new List<Sprite>();
    [SerializeField] List<Sprite> ThirdSprites = new List<Sprite>();
    public List<Tile> connectedTiles = new List<Tile>();
    public bool locked = false;
    [SerializeField] GridBase gridBase; 
    public enum TileType
    {
        Empty,
        Floor,
        Wall,
        Door,
        Entrance,
        Exit,
        Chest,
        LockedDoor,
    }
    public TileType type = TileType.Floor;
    [SerializeField] SpriteRenderer sr;
    // Start is called before the first frame update
    void Start()
    {
        
        sprites[0] = FirstSprites; sprites[1] = SecondSprites; sprites[2] = ThirdSprites;
        sr = this.gameObject.GetComponent<SpriteRenderer>();
        switch (type)
        {
            case TileType.Empty:
                Destroy(this.gameObject);
                break;
            case TileType.Floor:
                sr.sprite = sprites[gridBase.currentFloor-1][1];
                break;
            case TileType.Wall:
                sr.sprite = sprites[gridBase.currentFloor - 1][2];
                    break;
            case TileType.Door:
                sr.sprite = sprites[gridBase.currentFloor - 1][3];
                break;
            case TileType.Entrance:
                sr.sprite = sprites[gridBase.currentFloor - 1][4];
                break;
            case TileType.Exit:
                sr.sprite = sprites[gridBase.currentFloor - 1][5];
                break;
            case TileType.Chest:
                sr.sprite = sprites[gridBase.currentFloor - 1][6];
                break;
            case TileType.LockedDoor:
                sr.sprite = sprites[gridBase.currentFloor - 1][7];
                break;
        }
        gameObject.transform.Translate(new Vector3(0, 0, 1f));
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
            sr.color = new Color(0.6f, 0.6f, 0.6f);
        else if (highlighted)
            sr.color = new Color(0.8f, 0.8f, 0.8f);
        else sr.color = Color.white;
    }

    private void OnMouseEnter()
    {
        highlighted = true;
    }

    private void OnMouseExit()
    {
        highlighted = false;
    }

    private void OnMouseDown()
    {
        if (gridBase.clickable)
        {
            selected = !selected;
            if (selected) gridBase.centerCamera(this);
        }
    }
}
