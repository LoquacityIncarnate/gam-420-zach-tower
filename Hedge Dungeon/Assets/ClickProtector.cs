using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ClickProtector : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IPointerExitHandler
{
    public bool highlighted;
    public void OnPointerEnter(PointerEventData eventData)
    {
        highlighted = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        highlighted = false;
    }
    public void OnSelect(BaseEventData eventData)
    {
        //do your stuff when selected
    }
}
