using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class GridBase : MonoBehaviour
{
    [SerializeField] Vector2 gridSize = new Vector2( 10, 10 );
    [SerializeField] GameObject roomTemplate;
    [SerializeField] GameObject gridTile;
    [SerializeField] Camera mainCamera;
    [SerializeField] float camSpeed;
    Vector3 cameraTarget; bool movingCam = false;
    Room entrance, exit; public int connectionWeight = 3;
    int numLoot = 2; bool generating = false;
    public int currentFloor = 1; int whileCounter = 0;
    public bool movingUp = false; public Vector3 startCoord = new Vector3(1000,1000,0);
    [SerializeField] int seed; [SerializeField] bool randomSeed = true;
    [SerializeField] PlayerController player; Tile selection;
    Room[,] rooms; List<Tile> tiles;
    List<Room> mainPath = new List<Room>(); List<Room> connectedRooms = new List<Room>();
    [SerializeField] TMPro.TMP_InputField seedInput; [SerializeField] Toggle seedToggle; [SerializeField] Toggle keyToggle;
    [SerializeField] List<ClickProtector> menuItems = new List<ClickProtector>();
    public bool clickable = true;
    // Start is called before the first frame update
    void Start()
    {
        cameraTarget = mainCamera.transform.position;
        if (randomSeed) seed = Random.Range(0,10000000);
        UnityEngine.Random.seed = seed;
        StartCoroutine(generateRooms());
        player.transform.position = startCoord;
    }

    // Update is called once per frame
    void Update()
    {
        clickable = true;
        foreach (ClickProtector c in menuItems)
        {
            if (c.highlighted) clickable = false;
        }
        if (keyToggle.isOn)
        {
            player.key = true;
        }
        else player.key = false;
        if (!generating)
        {
            if (movingCam)
            {
                if (Vector3.SqrMagnitude(cameraTarget - mainCamera.transform.position) > 0.1)
                    mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position, cameraTarget, camSpeed);
                else movingCam = false;
            }
            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x + (Input.GetAxis("Horizontal") * camSpeed / 2), mainCamera.transform.position.y + (Input.GetAxis("Vertical") * camSpeed / 2), mainCamera.transform.position.z);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                movingCam = true;
            }
            if (Input.GetKeyDown(KeyCode.Period))
            {
                mainCamera.orthographicSize--;
            }
            if (Input.GetKeyDown(KeyCode.Comma))
            { mainCamera.orthographicSize++; }
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
                movingCam = false;
            /*if (Input.GetKeyDown(KeyCode.R))
            {
                deleteFloor();
                if (randomSeed) seed = Random.Range(0, 10000000);
                UnityEngine.Random.seed = seed; movingUp = false;
                StartCoroutine(generateRooms());
                player.transform.position = startCoord;
            }*/
            if (Input.GetKeyDown(KeyCode.K))
            {
                deleteFloor();
            }
        }
    }
    void deleteFloor()
    {
        mainPath.Clear();
        connectedRooms.Clear();
        if(tiles!=null)
        if(tiles.Count>0)
        foreach (Tile t in tiles)
        {
                if(t!=null)
            Destroy(t.gameObject);
        }
        if(rooms!=null)
        if(rooms.Length>0)
        foreach (Room r in rooms)
        {
                if(r!=null)
            Destroy(r.gameObject);
        }
    }
    IEnumerator generateRooms()
    {
        generating = true;
        if (gridSize.x < 3) gridSize.x = 3; if (gridSize.y < 3) gridSize.y = 3;
        if (currentFloor == 1)
        {
            roomTemplate.GetComponent<Room>().dimensions = new int[] { 6, 6 };
            gridSize.x = 6; gridSize.y = 6;
            connectionWeight = 20;
            numLoot = 2;
        }
        if (currentFloor == 2)
        {
            roomTemplate.GetComponent<Room>().dimensions = new int[] { 7, 5 };
            gridSize.x = 5; gridSize.y = 7;
            connectionWeight = 30;
            numLoot = 3;
        }
        if (currentFloor == 3)
        {
            roomTemplate.GetComponent<Room>().dimensions = new int[] { 9, 9 };
            gridSize.x = 4; gridSize.y = 4;
            connectionWeight = 20;
            numLoot = 3;
        }
        bool sufficient = false;
        whileCounter = 0;
        switch (currentFloor) {
            case <4:
                while (!sufficient)
                {
                    if (whileCounter > 100)
                    {
                        Debug.LogError("While loop got stuck.");
                        break;
                    }
                    whileCounter++;
                    deleteFloor();
                    rooms = new Room[Mathf.RoundToInt(gridSize.x), Mathf.RoundToInt(gridSize.y)];
                    List<Room> edgeRooms = new List<Room>(); tiles = new List<Tile>();

                    for (int i = 0; i < gridSize.x; i++)
                        for (int j = 0; j < gridSize.y; j++)
                        {
                            GameObject roomObj = Instantiate(roomTemplate, new Vector3((transform.position.x + roomTemplate.GetComponent<Room>().dimensions[0] * (-gridSize.x / 2) + (roomTemplate.GetComponent<Room>().dimensions[0] * i))-i, (transform.position.y + roomTemplate.GetComponent<Room>().dimensions[1] * ((-gridSize.y / 2)) + (roomTemplate.GetComponent<Room>().dimensions[1] * j))-j, transform.position.z), transform.rotation);
                            Room newRoom = roomObj.GetComponent<Room>();
                            newRoom.seed = seed + (2 * i) + (3 * j);
                            if ((i + j) % 2 != 0)
                                newRoom.hasDoors = true;
                            else newRoom.hasDoors = false;
                            if (i == gridSize.x - 1)
                                newRoom.rightSide = true;
                            else newRoom.rightSide = false;
                            if (j == 0)
                                newRoom.topSide = true;
                            else newRoom.topSide = false;
                            if (roomObj != null)
                            {
                                rooms[i, j] = newRoom;
                                newRoom.dimensions = new int[] { roomTemplate.GetComponent<Room>().dimensions[0], roomTemplate.GetComponent<Room>().dimensions[1] };
                                newRoom.type = Room.RoomType.Basic;
                                if (i == 0 || i == gridSize.x - 1 || j == 0 || j == gridSize.y - 1)
                                {
                                    if (!edgeRooms.Contains(newRoom)) edgeRooms.Add(newRoom);
                                }
                            }
                            if (i > 0)
                                if (rooms[i - 1, j] != null)
                                {
                                    newRoom.adjacentRooms[1] = (rooms[i - 1, j]); rooms[i - 1, j].adjacentRooms[3] = (newRoom);
                                }
                                else
                                {
                                    newRoom.adjacentRooms[1] = null;
                                }
                            if (j > 0)
                                if (rooms[i, j - 1] != null)
                                {
                                    newRoom.adjacentRooms[0] = (rooms[i, j - 1]); rooms[i, j - 1].adjacentRooms[2] = (newRoom);
                                }
                                else
                                {
                                    newRoom.adjacentRooms[0] = null;
                                }
                            if (i < gridSize.x - 1)
                                if (rooms[i + 1, j] != null)
                                {
                                    newRoom.adjacentRooms[3] = (rooms[i + 1, j]); rooms[i + 1, j].adjacentRooms[1] = (newRoom);
                                }
                                else
                                {
                                    newRoom.adjacentRooms[3] = null;
                                }
                            if (j < gridSize.y - 1)
                                if (rooms[i, j + 1] != null)
                                {
                                    newRoom.adjacentRooms[2] = (rooms[i, j + 1]); rooms[i, j + 1].adjacentRooms[0] = (newRoom);
                                }
                                else
                                {
                                    newRoom.adjacentRooms[2] = null;
                                }
                        }
                    int entranceSpot = Random.Range(0, edgeRooms.Count);
                    if (edgeRooms[entranceSpot] != null)
                    {
                        entrance = edgeRooms[entranceSpot];
                        entrance.type = Room.RoomType.Entrance;
                    }
                    bool needsExit = true;
                    while (needsExit)
                    {
                        whileCounter++;
                        int exitDistance = Random.Range(1, edgeRooms.Count - 1);
                        exitDistance += entranceSpot;
                        if (exitDistance >= edgeRooms.Count) exitDistance -= edgeRooms.Count;
                        if (edgeRooms[exitDistance] != null)
                        {
                            needsExit = false;
                            exit = edgeRooms[exitDistance];
                            foreach (Room r in edgeRooms[exitDistance].adjacentRooms)
                            {
                                if (r == entrance)
                                    needsExit = true;
                                if (r != null)
                                    foreach (Room ro in r.adjacentRooms)
                                    {
                                        if (ro == entrance)
                                            needsExit = true;
                                    }
                            }
                        }
                        if (whileCounter > 100)
                        {
                            Debug.LogError("While loop got stuck, exit was trying to place at " + edgeRooms[exitDistance].transform.position + " among " + edgeRooms.Count);
                            break;
                        }

                    }
                    exit.type = Room.RoomType.Exit;
                    mainPath = pathFinder(false);
                    if (mainPath.Count > (gridSize.x + gridSize.y) / 2)
                        sufficient = true;
                    else
                    {
                        mainPath = pathFinder(false);
                        if (mainPath.Count - 1 > (gridSize.x + gridSize.y) / 2)
                            sufficient = true;
                        else
                        {
                            Debug.Log("Path not long enough. Regenerating...");
                            yield return null;
                        }
                    }
                    while (connectedRooms.Count < ((gridSize.x * gridSize.y) * connectionWeight / 100))
                        foreach (Room r in mainPath)
                        {
                            if (!connectedRooms.Contains(r)) connectedRooms.Add(r);
                            connectRooms(r, connectionWeight);
                        }
                    List<Room> shortestPath = pathFinder(true);
                    if (shortestPath.Count - 1 > (gridSize.x + gridSize.y) / 2 + (gridSize.x + gridSize.y) / 4)
                    {
                        sufficient = true; //Debug.Log(connectedRooms.Count + "/" + (gridSize.x * gridSize.y) + "=" + connectedRooms.Count / (gridSize.x * gridSize.y));
                    }
                    else
                    {
                        Debug.Log("Shortest path not long enough. Regenerating..."); sufficient = false;
                        yield return null;
                    }
                    if (sufficient ==false)
                    {
                        Debug.Log("Insufficient generation.");
                        UnityEngine.Random.seed++;
                        yield return null;
                    }
                }
                if (numLoot > 0) {
                    int lootTracker = numLoot;
                    List<Room> potentialLoot = new List<Room>();
                    List<Room> potentialLocked = new List<Room>();
                    foreach (Room r in rooms)
                    {
                        if (r.connectedRooms.Count > 0)
                        {
                            if (!mainPath.Contains(r) && r.type == Room.RoomType.Basic)
                                potentialLoot.Add(r);
                            if (!mainPath.Contains(r) && r.connectedRooms.Count == 1 && r.type == Room.RoomType.Basic)
                                potentialLocked.Add(r);
                        }
                    }
                    for (int i = 0; i < numLoot; i++)
                    {
                        if (potentialLoot.Count > 0)
                        {
                            if (potentialLocked.Count > 0)
                                if (Random.Range(0, 1) > 0.5f)
                                { int n = Random.Range(0, potentialLoot.Count); potentialLoot[n].type = Room.RoomType.Loot; potentialLocked.Remove(potentialLoot[n]); potentialLoot.RemoveAt(n);  }
                                else { int n = Random.Range(0, potentialLocked.Count); potentialLocked[n].type = Room.RoomType.Locked; potentialLoot.Remove(potentialLocked[n]); potentialLocked.RemoveAt(n); }
                            else { int n = Random.Range(0, potentialLoot.Count);  potentialLoot[n].type = Room.RoomType.Loot; potentialLocked.Remove(potentialLoot[n]); potentialLoot.RemoveAt(n); }
                        }
                    }
                }
                foreach (Room r in rooms)
                {
                    if (r.connectedRooms.Count < 1)
                    {
                        if(r.adjacentRooms[1]!=null)r.adjacentRooms[1].adjacentRooms[3] = null;
                        if (r.adjacentRooms[2] != null) r.adjacentRooms[2].adjacentRooms[0] = null;
                        if (r.adjacentRooms[0] != null) r.adjacentRooms[0].adjacentRooms[2] = null;
                        if (r.adjacentRooms[3] != null) r.adjacentRooms[3].adjacentRooms[1] = null;
                        {
                                Destroy(r.gameObject);
                        }
                    }
                    if(currentFloor==1 || currentFloor==3)
                    if (r.connectedRooms.Count > 1 && r.type == Room.RoomType.Basic)
                    {
                        int n = Random.Range(0, r.connectedRooms.Count); if (n == 0) r.type = Room.RoomType.Corridor;
                    }
                }
                foreach (Room r in connectedRooms)
                {
                    if (r.connectedRooms.Count > 0)
                    {
                        r.GenerateRoom();
                        for (int i = 0; i < r.dimensions[0]; i++)
                        {
                            for (int j = 0; j < r.dimensions[1]; j++)
                            {
                                Tile newTile = Instantiate(gridTile, new Vector3(r.transform.position.x + (-r.dimensions[0] / 2) + i, r.transform.position.y + (-r.dimensions[1] / 2) + j, r.transform.position.z), r.transform.rotation).GetComponent<Tile>();
                                newTile.type = r.layout[i, j].type; tiles.Add(newTile);
                                if (r.type == Room.RoomType.Locked) newTile.locked = true;
                                if (newTile.type == Tile.TileType.Entrance && !movingUp)
                                {
                                    startCoord = newTile.transform.position; mainCamera.transform.position = new Vector3(newTile.transform.position.x, newTile.transform.position.y, newTile.transform.position.z - 10);
                                    player.transform.position = startCoord;
                                }
                                if (newTile.type == Tile.TileType.Exit && movingUp)
                                {
                                    startCoord = newTile.transform.position; mainCamera.transform.position = new Vector3(newTile.transform.position.x, newTile.transform.position.y, newTile.transform.position.z - 10);
                                    player.transform.position = startCoord;
                                }
                                foreach (Tile t in tiles)
                                {
                                    if(newTile.type!=Tile.TileType.Wall)
                                    if (Vector3.SqrMagnitude(newTile.transform.position - t.transform.position) <= 2 && t.gameObject != newTile.gameObject)
                                    {
                                        if (!newTile.connectedTiles.Contains(t))
                                            newTile.connectedTiles.Add(t);
                                        if (!t.connectedTiles.Contains(newTile))
                                            t.connectedTiles.Add(newTile);
                                    }
                                }
                            }
                        }
                    }
                }
                generating = false;
                break;
            case >=4:
                int roomsRequired = Mathf.FloorToInt((gridSize.x * gridSize.y) / 3);
                for (int i = 0; i < roomsRequired; i++)
                {
                    GameObject roomObj = Instantiate(roomTemplate, new Vector3(transform.position.x + Random.Range(-2,2), transform.position.y + Random.Range(-2, 2), transform.position.z), transform.rotation);
                    Room newRoom = roomObj.GetComponent<Room>();
                    Rigidbody2D rb = roomObj.GetComponent<Rigidbody2D>();

                }
                generating = false;
                break;
        }
    }
    public void centerCamera(Tile t)
    {
        if (t.type != Tile.TileType.Wall)
            if (t.locked)
            {
                if (player.key == true)
                {
                    player.transform.position = t.transform.position;
                }
            }
        else
        player.transform.position = t.transform.position;
        if (t.type == Tile.TileType.Entrance && currentFloor >1)
        {
            deleteFloor();
            currentFloor --; UnityEngine.Random.seed = seed; movingUp = true;
            StartCoroutine(generateRooms());
            player.transform.position = startCoord;
        }
        else if (t.type == Tile.TileType.Exit && currentFloor <3)
        {
            deleteFloor();
            currentFloor++; UnityEngine.Random.seed = seed; movingUp = false;
            StartCoroutine(generateRooms());
            player.transform.position = startCoord;
        }
        else {
            cameraTarget = new Vector3(t.gameObject.transform.position.x, t.gameObject.transform.position.y, mainCamera.transform.position.z);
            movingCam = true;
            if(selection!=null)
            selection.selected = false;
            selection = t;
            if (t.type == Tile.TileType.Chest)
                OpenLoot(t);
        }
    }

    public void Generate()
    {
        if (seedToggle.isOn)
        {
            seed = Random.Range(0, 10000000);
            seedInput.text = seed.ToString();
        }
        else if (int.TryParse(seedInput.text, out int newSeed))
        { 
            seed = newSeed;
        }
        UnityEngine.Random.seed = seed; movingUp = false;
        StartCoroutine(generateRooms());
        player.transform.position = startCoord;
    }
    void OpenLoot(Tile t)
    {
        t.type = Tile.TileType.Floor;
        Debug.Log("Loot opened");
    }
    void connectRooms(Room start, int weight)
    {
        if (weight > 0)
        {
            foreach (Room r in start.adjacentRooms)
            {
                if (r != null)
                {
                    int newConnection = Random.Range(-20, 100);
                    if (newConnection+(connectedRooms.Count/((gridSize.x+gridSize.y))) < weight && !start.connectedRooms.Contains(r) && !connectedRooms.Contains(r))
                    {
                        start.connectedRooms.Add(r);
                        r.connectedRooms.Add(start);
                        connectedRooms.Add(r);
                        connectRooms(r, weight-5);
                    }
                    if (newConnection/2 + (connectedRooms.Count / ((gridSize.x+gridSize.y))) + start.connectedRooms.Count * 5 < weight && !start.connectedRooms.Contains(r) && connectedRooms.Contains(r))
                    {
                        start.connectedRooms.Add(r);
                        r.connectedRooms.Add(start);
                        connectRooms(r, weight-5);
                    }
                }
            }
        }
        else return;
    }
    List<Room> pathFinder(bool ConnectedOnly)
    {
        List<Room> path = new List<Room>(); List<Room> frontier = new List<Room>(); List<Room> visited = new List<Room>();
        frontier.Add(entrance);
        int counter = 0;
        while (frontier.Count > 0)
        {
            counter++;
            if (counter >= 100)
            {
                Debug.Log("While counter in pathfinder got stuck.");
                break;
            }
            Room tempR = frontier[0]; visited.Add(tempR);
            if (!ConnectedOnly)
                foreach (Room r in tempR.adjacentRooms)
                {
                    if (r != null)
                        if (!visited.Contains(r))
                        {
                            r.previousRoom = tempR;
                            int rand = Random.Range(0, 5);
                            if (rand > 4)
                                frontier.Add(r);
                            else
                                frontier.Insert(Random.Range(0, frontier.Count - 1), r);
                            visited.Add(r);
                            if (r == exit)
                            {
                                frontier.Clear();
                                break;
                            }
                        }
                }
            else
            {
                foreach (Room r in tempR.connectedRooms)
                {
                    if (r != null)
                        if (!visited.Contains(r))
                        {
                            r.previousRoom = tempR;
                            frontier.Add(r);
                            visited.Add(r);
                            if (r == exit)
                            {
                                frontier.Clear();
                                break;
                            }
                        }
                }
            }
            frontier.Remove(tempR);
        }
        Room temp = exit;
        if (exit != null)
        {
            int aCounter = 0;
            while (temp != entrance)
            {
                aCounter++;
                if (aCounter >= 100)
                {
                    Debug.Log("While counter in pathfinder got stuck placing entrance.");
                    break;
                }
                path.Add(temp);
                if(!temp.connectedRooms.Contains(temp.previousRoom))
                temp.connectedRooms.Add(temp.previousRoom);
                if(!temp.previousRoom.connectedRooms.Contains(temp))
                temp.previousRoom.connectedRooms.Add(temp);
                temp = temp.previousRoom;
                
            }
        }
        path.Add(temp);
        path.Reverse();
        string printout = "Path: ";
        foreach (Room r in path)
            printout += r.transform.position + " ";
        //Debug.Log(printout);
        return path;
    }
}
