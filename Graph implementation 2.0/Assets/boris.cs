﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UnityEngine;

public class Boris
{
    float DotProduct(float[,] avec, float[,] toi) //Test if this correctly returns dot product
    {
        if (avec.GetLength(1) != toi.GetLength(0))
        { throw new ArgumentException("Array dimensions won't work for dot product"); }
        int rows = avec.GetLength(0); int col = toi.GetLength(1); float result = 0;
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < col; j++)
                result += avec[i, j] * toi[j, i];
        return result;
    }
    float Mag(float[,] vec)
    {
        float magnitude = Mathf.Sqrt(DotProduct(vec, vec));
        return magnitude;
    }

    //Never referenced:
    /*float[,] crt2sph(float[,] x_ijk)
    {
        if (x_ijk.Length(1) < 3)
            return null;
        float[x_ijk.Length(0), x_ijk.Length(1)] r_ijk = new float[x_ijk.Length(0), x_ijk.Length(1)];
        float sum = 0;
        for (int i = 0; i < x_ijk.Length(1); i++)
        { float temp = 0;
            for (int j = 0; j < x_ijk.Length(0); j++)
                temp += x_ijk[i, j];
            sum += temp * temp;
        }
        r_ijk[:, 0] = Math.Sqrt(sum);
        r_ijk[:, 1] = Math.Acos(x_ijk[:,2/r_ijk[:,0]);
        r_ijk[:, 2] = Math.Atan2(x_ijk[:,1], x_ijk[:,0]);
        return r_ijk;
    }
    */
    /*float[,] sph2crt(float[,] r_ijk)
{ float[r_ijk.Length(0), r_ijk.Length(1)] x_ijk = new float[r_ijk.Length(0), r_ijk.Length(1)];
    return x_ijk;}*/
    /*Vector3 sph_to_crt(Vector3 vector)
    {Vector3 crt = new Vector3();
        return crt;}*/
    Vector3 crt_to_sph(Vector3 vector)
    {
        Vector3 sph = new Vector3();
        sph.x = Mathf.Sqrt(vector.x*vector.x+vector.y*vector.y+vector.z*vector.z);
        sph.y = Mathf.Acos(vector.z/sph.x);
        sph.z = Mathf.Atan2(vector.y, vector.x);
        return sph;
    }
    float[] boris(int tf, float[,] r0, float[,] v0, float m, float q, float gs, float kp, bool tdir = true, float rmax = 10, float dn_save =1, float dt_wci = 0.01f) 
    {
        /*
        Boris particle pusher tracing code.

    Args:
        tf (int): total time in units [s]
        dt (int): timestep incrementation
        r0 (ndarray): initial position array in units [RE]
        v0 (ndarray): initial velcoity array in units [m/s]
        m (float): particle mass in units [kg]
        q (float): particle charge in units [C]
        gs (float): shielding constant
        kp (float): kp index
        tdir (bool, optional): specifies forward tracing ['fw'] or backward tracing ['bw'].
                                Defaults to 'fw.'
        rmax (float, optional): maximum radial distance a particle is allowed to reach

    Returns:
        tdat (ndarray): time data in units [s]
        tdrift (ndarray): drift time in units [s]
        vdat (ndarray): velocity data (spherical) in units [m/s]
        rdat (ndarray): position data (spherical) in units [m]
        emag (ndarray): magnitude of total E-field in units [mV/m]
         */
        int RE = 6371000;

        float gyroperiod 
            = 0;// (2 * Math.PI) / (Math.Abs(q) * Mag(B_dipole(r0*RE)/m)); //B_dipole is a method from bfields
        float dt = dt_wci * gyroperiod;
        int steps = Mathf.RoundToInt(tf / dt);
        int nout = Mathf.FloorToInt(steps / dn_save);

        Debug.Log("Run time: " + tf + " Time step: " + dt + " Steps: " + steps);
        float[] tdat = new float[nout];
        float[,] rdat = new float[nout, 3];
        float[,] vdat = new float[nout, 3];
        float[] emag = new float[nout];

        tdat[0] = 0;
        rdat[0,0] = r0[0,0]; rdat[0, 1] = r0[0, 1]; rdat[0, 2] = r0[0, 2];
        vdat[0, 0] = v0[0, 0]; vdat[0, 1] = v0[0, 1]; vdat[0, 2] = v0[0, 2];
        int isave = 1;
        float[,] rnew = r0;
        float[,] vnew = v0;
        float n;
        if (tdir)
            n = 1.0f;
        else
            n = -1.0f;

        float[] output = new float[] {0};
        return output;
    }
}
/*
 import numpy as np #imports a mathematical python package
import math #imports a basic math package
from bfields import * #dependent on bfields
from efields import * #dependent on efields
from plottools import * #dependent on plottools
from tqdm import tqdm #package that makes loops show progress bars


# compute vector of numpy array
def mag(vec):
    magnitude = np.sqrt(np.dot(vec, vec))
    return magnitude


def crt2sph(x_ijk): #takes in a cartesian matrix
    r_ijk = np.zeros_like(x_ijk) #makes a zero-filled matrix with the same dimensions
    r_ijk[:,0] = np.sqrt(np.sum(x_ijk**2, axis=1)) #set the first column of the new equal to the square root of the sum of the input matrix squared's rows
    r_ijk[:,1] = np.arccos(x_ijk[:,2] / r_ijk[:,0]) #set the second column of the new equal to the arccos of (input's third column divided by empty's first column)
    r_ijk[:,2] = np.arctan2(x_ijk[:,1], x_ijk[:,0]) #set the third column of the new equal to the signed arctangent of the ratio between input's second and first columns
    return r_ijk #returns the new spherical matrix

# convert cartesian to spherical coordinates
def crt_to_sph(x, y, z):
    """
    (x, y, z) to (r, theta, phi)
    r:      radial distance
    theta:  polar angle (radians)
    phi:    azimuthal angle (radians)
    """
    r = np.sqrt(x**2 + y**2 + z**2)  # radial distance
    theta = np.arccos(z / r)  # polar angle
    phi = np.arctan2(y, x)  # azimuthal angle

    return r, theta, phi

def sph2cart(r_ijk): #takes in a spherical coordinates matrix
    x_ijk = np.zeros_like(r_ijk) #makes a zero-filled matrix with the same dimensions
    x_ijk[:,0] = r_ijk[:,0] * np.sin(r_ijk[:,1]) * np.cos(r_ijk[:,2]) #set the first column of the new equal to input's first column times sin(second column) times cos(third column)
    x_ijk[:,1] = r_ijk[:,0] * np.sin(r_ijk[:,1]) * np.sin(r_ijk[:,2]) #set the second column of the new equal to input's first column times sin(second column) times sin(third column)
    x_ijk[:,2] = r_ijk[:,0] * np.cos(r_ijk[:,1]) #set the third column of the new equal to the input's first column times cos(second column)
    return x_ijk #returns the new cartesian matrix

# convert spherical to cartesian coordinates
def sph_to_crt(r, theta, phi):
    """
    (r, theta, phi) to (x,y,z)
    r:      radial distance
    theta:  polar angle (radians)
    phi:    azimuthal angle (radians)
    """
    x = r * np.sin(theta) * np.cos(phi)  # x-coordinate
    y = r * np.sin(theta) * np.sin(phi)  # y-coordinate
    z = r * np.cos(theta)  # z-coordinate

    return x, y, z


def boris(tf, r0, v0, m, q, gs, kp, tdir="fw", rmax=10, dn_save=1, dt_wci=0.01):
    """
    Boris particle pusher tracing code.

    Args:
        tf (int): total time in units [s]
        dt (int): timestep incrementation
        r0 (ndarray): initial position array in units [RE]
        v0 (ndarray): initial velcoity array in units [m/s]
        m (float): particle mass in units [kg]
        q (float): particle charge in units [C]
        gs (float): shielding constant
        kp (float): kp index
        tdir (bool, optional): specifies forward tracing ['fw'] or backward tracing ['bw'].
                                Defaults to 'fw.'
        rmax (float, optional): maximum radial distance a particle is allowed to reach

    Returns:
        tdat (ndarray): time data in units [s]
        tdrift (ndarray): drift time in units [s]
        vdat (ndarray): velocity data (spherical) in units [m/s]
        rdat (ndarray): position data (spherical) in units [m]
        emag (ndarray): magnitude of total E-field in units [mV/m]
    """
    RE = 6371000 #Earth's approximate radius
    
    # calculate stepsize (dt) to be no bigger than half the gyroperiod
    gyroperiod = (2*np.pi) / ((abs(q) * mag(B_dipole(r0*RE))) / m) #gyroperiod is 2pi/((|particle charge|*the magnitude of earth's magnetic dipole relative to the particle's coordinates relative to Earth)/particle mass)
    dt = dt_wci * gyroperiod  #step size equals the input step size times the gyroperiod
    steps = int(tf / dt) #number of steps equals the total time divided by the step size
    nout = steps // dn_save #divides the steps by dn_save, discarding any remainder

    print('Run time: {0}, Time step: {1}, Steps: {2}'
          .format(tf, dt, steps)) #prints the total run time, the length of each step, and the number of steps

    # old
    tdat = np.zeros((nout,)) #creates a zero-filled time data 1D array with a length equal to the number of steps
    rdat = np.zeros((nout, 3)) * np.nan #creates a zero-filled position data 2D array with [STEPS] rows and 3 columns, and fills all entries with null
    vdat = np.zeros((nout, 3)) #creates a zero-filled velocity data 2D array with [STEPS] rows and 3 columns
    emag = np.zeros((nout,))  #creates a zero-filled electromagnetic magnitude 1D array with a length equal to the number of steps

    # tdat = np.array([np.nan] * steps - 1)
    # rdat = np.array(([np.nan] * steps), 3)
    # vdat = np.array(([np.nan] * steps), 3)
    # emag = np.array([np.nan] * steps)

    # set initial conditions
    tdat[0] = 0
    rdat[0] = r0  # [RE]
    vdat[0] = v0  # [RE/s]
    isave = 1     # We have already saved the first data point at t=0
    rnew = r0
    vnew = v0

    # forward vs. backward tracing
    if tdir == "fw":
        n = 1.0
    else:
        n = -1.0

    for i in tqdm(range(0, steps - 1)): #for each step
        # print('{0} of {1}; Saving {2} of {3}'.format(i, steps, int(i // dn_save), nout))

        # set current position and velocity (cartesian coords)
        r = rnew # rdat[i]
        v = vnew # vdat[i]

        # compute B-field [T]
        B0 = B_dipole(r*RE, sph=False)
        # test function: no B-field
        # B0 = np.array([0.0, 0.0, 0.0])

        # print("r0, v0", r, v)
        # print("B", B0)

        # compute convection E-field [mV/m]
        EC = vs_efield(r, gs, kp, sph=False)

        # compute corotation E-field [mV/m]
        ER = corotation_efield(r, sph=False)

        # compute total E-field and covert to [V/m]
        # E0 = np.add(EC, ER) * 1e-3
        E0 = np.array([0.0, 0.0, 0.0])

        # c0, ax, bx - arbitrary; to break down equation
        c0 = (dt * q * B0) / (2 * m)

        # push step 1 - update velocity with half electrostatic contribution
        v1 = v + (n * (q * E0 * dt) / (2 * m))

        # push step 2 - rotated via the magnetic field contribution
        ax = v1 + (n * np.cross(v1, c0))
        bx = (2 * c0) / (1 + (n * c0**2))
        v2 = v1 + (n * np.cross(ax, bx))

        # push step 3 - updated again with another half of the electrostatic push [m/s]
        vnew = v2 + (n * (q * E0 * dt) / (2 * m))

        # update position [RE]
        rnew = r + (n * (vnew * dt) / RE)

        # Append to data arrays
        #   - Iteration i creates data for i+1
        if ((i+1) % dn_save) == 0:
            # print('Saving {0} of {1}'.format(i // dn_save, nout))
            tdat[isave] = (i+1) * dt  # if n == 1.0 else tf - i * dt  # time [s]
            vdat[isave] = vnew  # velocity [m/s]
            rdat[isave] = rnew  # position [RE]
            emag[isave] = mag(E0 / 1e-3)  # magnitude of total E-field [mV/m]
            isave += 1


        # find positional magnitude
        rmag = np.sqrt(np.dot(rnew, rnew))
        # check if particle(s) crossed rmax
        if rmag >= rmax:
            tdrift = (i + 1) * dt #drift time is the number of the next step times the step size
            break
        else:
            tdrift = np.nan
    # """

    # Trim the data if rmag > rmax
    if isave < nout:
        tdat = tdat[:isave]
        rdat = tdat[:isave,:]
        vdat = tdat[:isave,:]
        edat = tdat[:isave,:]

    # tdrift = 1.0

    return tdat, tdrift, vdat, rdat, emag

 */
