using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    [SerializeField] private Graph theGraph;
    public List<string> connections;
    public Vector3 pos;
    public float weight;
    public PathNode parentNode;
    bool awakened;
    
    // Start is called before the first frame update
    void Awake()
    {
        pos = this.gameObject.transform.position;
        theGraph.AddNode(this);
        awakened = false;
    }
}
