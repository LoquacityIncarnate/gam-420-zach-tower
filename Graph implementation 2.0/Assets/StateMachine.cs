using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    public abstract void Enter(StateMachine sm);
    public abstract void Update(StateMachine sm);
    public abstract void Exit(StateMachine sm);
}
public class Wandering : State
{
    public override void Enter(StateMachine sm)
    {
        sm.renderer.material = sm.a;
        Debug.Log("Entering Wander");
        float distance = Vector3.SqrMagnitude(sm.graph.nodes[0].pathNode.pos - sm.transform.position); Graph.Node closest = sm.graph.nodes[0];
        foreach (Graph.Node n in sm.graph.nodes)
        {
            if (Vector3.SqrMagnitude(n.pathNode.pos - sm.transform.position) < distance)
            {
                closest = n;
                distance = Vector3.SqrMagnitude(n.pathNode.pos - sm.transform.position);
            }
        }
        sm.target = closest.pathNode.pos; sm.currentNode = closest.pathNode;
        
    }
    public override void Update(StateMachine sm)
    {
        if (sm.progress < sm.route.Count)
        {
            if (Vector3.SqrMagnitude(sm.transform.position - sm.target) > 0.01f)
            {
                sm.transform.position = Vector3.MoveTowards(sm.transform.position, sm.target, sm.graph.stateSpeed * Time.deltaTime);
                sm.currentNode = sm.route[sm.progress];
            }
            else
            {
                sm.progress++;
                if (sm.progress < sm.route.Count) sm.target = sm.route[sm.progress].pos;
            }
        }
        else
        {
            Graph g = GameObject.FindFirstObjectByType<Graph>();
            sm.route = g.HeuristicSearch(sm.currentNode, g.nodes[Random.Range(0,g.nodes.Count-1)].pathNode);
            sm.progress = 0; sm.renderer.material = sm.a;
        }
        sm.cd -= Time.deltaTime;
        if(sm.cd<=0)
        foreach (AIController nav in sm.graph.navigators)
        {
            if (Vector3.SqrMagnitude(sm.transform.position - nav.transform.position) < 2)
            {
                sm.ai = nav;
                sm.SwapState(new Chasing());
            }
        }
        foreach (StateMachine sam in sm.graph.staters)
        {
            if(sm!=sam && sm.cd<=0)
            if (Vector3.SqrMagnitude(sm.transform.position - sam.transform.position) < 2)
            {
                sm.route.Reverse();
                    sm.renderer.material = sm.b;
                    int temp = sm.progress;
                sm.progress = sm.route.Count - sm.progress ;
                    sm.cd = 1;
            }
        }
    }
    public override void Exit(StateMachine sm)
    {
        sm.route.Clear(); sm.progress = 0; sm.renderer.material = sm.a;
    }
}
public class Stopped : State
{
    public override void Enter(StateMachine sm)
    {
        sm.timer = 2;
        Debug.Log("Entering Stopped");
    }
    public override void Update(StateMachine sm)
    {
        sm.timer -= Time.deltaTime; sm.cd -= Time.deltaTime;
        if(sm.cd<=0)
        foreach (AIController nav in sm.graph.navigators)
        {
            if (Vector3.SqrMagnitude(sm.transform.position - nav.transform.position) < 3)
            {
                sm.ai = nav;
                sm.SwapState(new Chasing());
            }
        }
        if (sm.timer <= 0)
        {
            sm.SwapState(new Wandering()); sm.timer = 1;
        }
    }
    public override void Exit(StateMachine sm)
    {
        
    }
}
public class Chasing : State
{
    public override void Enter(StateMachine sm)
    {
        sm.timer = 5;
        Debug.Log("Entering Chasing");
    }
    public override void Update(StateMachine sm)
    {
        sm.timer -= Time.deltaTime;
        sm.transform.position = Vector3.MoveTowards(sm.transform.position, sm.ai.transform.position, sm.graph.navSpeed * Time.deltaTime);
        if (sm.timer <= 0)
        {
            sm.SwapState(new Stopped());
        }
    }
    public override void Exit(StateMachine sm)
    {
        sm.cd = 5;
    }
}

public class StateMachine : MonoBehaviour
{
    public enum AIPaths
    {
        BreadthFirst,
        Heuristic,
        Dijkstras,
        AStar,
    }
    public AIPaths myPath;
    public List<PathNode> route;
    public int progress;
    public Vector3 target;
    public Graph graph;
    public PathNode currentNode;
    public AIController ai;
    public float timer;
    public float cd = 5;
    public Material a, b;
    public MeshRenderer renderer;
    State currentState;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        SwapState(new Wandering());
    }
    void Update()
    {
        if (currentState != null) currentState.Update(this);
    }
    public void SwapState(State aState)
    {
        if (currentState != null) currentState.Exit(this);
        currentState = aState;
        currentState.Enter(this);
    }
    private void OnMouseDown()
    {
        SwapState(new Stopped());
    }
}
