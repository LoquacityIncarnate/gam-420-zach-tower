using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    List<PathNode> pathBreadth = new List<PathNode>();
    List<PathNode> pathHeuristic = new List<PathNode>();
    List<PathNode> pathDijkstras = new List<PathNode>();
    List<PathNode> pathAStar = new List<PathNode>();
    [SerializeField] List<GameObject> AIs = new List<GameObject>();
    public List<AIController> navigators = new List<AIController>();
    public List<StateMachine> staters = new List<StateMachine>();
    [SerializeField] GameObject test;
    public float navSpeed =2; public float stateSpeed;
    [SerializeField] bool fillDraw = false;
    [SerializeField] List<Polygon> polygons = new List<Polygon>();
    // This class represents a node in our graph
    public class Node
    {
        public PathNode pathNode;
        public List<Node> connectedNodes = new List<Node>();
    }
    public List<Node> nodes = new List<Node>();
    public Node youAreHere = null; Vector3 temp;
    void Awake()
    {
        if (AIs.Count > 0)
        {
            foreach (GameObject AI in AIs)
            {
                if (AI != null) {
                    if(AI.GetComponent<AIController>()!=null)
                    
                    navigators.Add(AI.GetComponent<AIController>());
                }
            }
        }
        GameObject[] polyObjects = GameObject.FindGameObjectsWithTag("walkable");
        foreach (GameObject g in polyObjects)
        {
            Polygon p = g.GetComponent<Polygon>();
            if (p != null)
                polygons.Add(p);
        }
    }
    // Add a new node to the graph
    public void AddNode(PathNode aPathNode)
    {
        Node n = new Node();
        n.pathNode = aPathNode;
        nodes.Add(n);
        n.pathNode.weight = Random.Range(0f, 1f);
        n.connectedNodes.Clear();
        n.pathNode.connections.Clear();
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i] != null)
            {
                Node aNode = nodes[i];
                if (Vector3.SqrMagnitude(n.pathNode.pos - aNode.pathNode.pos) < 2.75 && aNode.pathNode.gameObject.name != n.pathNode.gameObject.name)
                {
                    if (!n.connectedNodes.Contains(aNode)) { n.connectedNodes.Add(nodes[i]); n.pathNode.connections.Add(nodes[i].pathNode.gameObject.name); }
                    if (!aNode.connectedNodes.Contains(n)) { nodes[i].connectedNodes.Add(n); nodes[i].pathNode.connections.Add(n.pathNode.gameObject.name); }
                }
            }
        }

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PathNode startN = nodes[Random.Range(0, nodes.Count)].pathNode;
            PathNode endN = nodes[Random.Range(0, nodes.Count)].pathNode;
            pathBreadth = BreadthFirstSearch(startN, endN);
            pathHeuristic = HeuristicSearch(startN, endN);
            pathDijkstras = DijkstrasSearch(startN, endN);
            pathAStar = AStarSearch(startN, endN);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            location = new Vector3(location.x, location.y-15, location.z);
            Debug.Log("Registered click at "+ location.ToString());
            float distance = Vector3.SqrMagnitude(nodes[0].pathNode.pos-location); Node closest = nodes[0];
            foreach (Node n in nodes)
            {
                if (Vector3.SqrMagnitude(n.pathNode.pos - location) < distance)
                {
                    closest = n;
                    distance = Vector3.SqrMagnitude(n.pathNode.pos - location);
                }
            }
            Instantiate(test, closest.pathNode.pos, Quaternion.identity);
            if (navigators.Count > 0)
            {
                foreach (AIController nav in navigators)
                {
                    if (nav != null)
                    {
                        switch (nav.myPath)
                        {
                            case AIController.AIPaths.BreadthFirst:
                                nav.route = BreadthFirstSearch(nav.currentNode, closest.pathNode);
                                pathBreadth = nav.route;
                                nav.progress = 0;
                                break;
                            case AIController.AIPaths.Heuristic:
                                nav.route = HeuristicSearch(nav.currentNode, closest.pathNode);
                                pathHeuristic = nav.route;
                                nav.progress = 0;
                                break;
                            case AIController.AIPaths.Dijkstras:
                                nav.route = DijkstrasSearch(nav.currentNode, closest.pathNode);
                                pathDijkstras = nav.route;
                                nav.progress = 0;
                                break;
                            case AIController.AIPaths.AStar:
                                nav.route = AStarSearch(nav.currentNode, closest.pathNode);
                                pathAStar = nav.route;
                                nav.progress = 0;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
    private void OnDrawGizmos()
    {
        if(nodes!=null)
        if(nodes.Count>0)
        for (int i = 0; i < nodes.Count; i++)
        {
                    if (nodes[i] != null)
                    {
                        Gizmos.color = new Color(nodes[i].pathNode.weight, nodes[i].pathNode.weight, nodes[i].pathNode.weight);
                        temp = new Vector3(nodes[i].pathNode.gameObject.transform.position.x, nodes[i].pathNode.gameObject.transform.position.y + nodes[i].pathNode.weight/4, nodes[i].pathNode.gameObject.transform.position.z);
                        Gizmos.DrawSphere(temp, 0.2f);
                        Gizmos.color = Color.yellow;
                        for (int j = 0; j < nodes[i].connectedNodes.Count; j++)
                        {//Gizmos.DrawLine(nodes[i].pathNode.gameObject.transform.position, nodes[i].connectedNodes[j].pathNode.gameObject.transform.position);

                        }
                    }
        }
        if (pathBreadth.Count > 0)
            for (int i = 0; i < pathBreadth.Count; i++)
            {
                Gizmos.color = Color.red;
                temp = new Vector3(pathBreadth[i].pos.x, pathBreadth[i].pos.y+0.6f, pathBreadth[i].pos.z);
                Gizmos.DrawSphere(temp, 0.4f);
            }
        if (pathHeuristic.Count > 0)
            for (int i = 0; i < pathHeuristic.Count; i++)
            {
                Gizmos.color = Color.blue;
                temp = new Vector3(pathHeuristic[i].pos.x, pathHeuristic[i].pos.y + 0.8f, pathHeuristic[i].pos.z);
                Gizmos.DrawSphere(temp, 0.375f);
            }
        if (pathDijkstras.Count > 0)
            for (int i = 0; i < pathDijkstras.Count; i++)
            {
                Gizmos.color = Color.green;
                temp = new Vector3(pathDijkstras[i].pos.x, pathDijkstras[i].pos.y + 1.0f, pathDijkstras[i].pos.z);
                Gizmos.DrawSphere(temp, 0.35f);
            }
        if (pathAStar.Count > 0)
            for (int i = 0; i < pathAStar.Count; i++)
            {
                Gizmos.color = Color.magenta;
                temp = new Vector3(pathAStar[i].pos.x, pathAStar[i].pos.y + 1.2f, pathAStar[i].pos.z);
                Gizmos.DrawSphere(temp, 0.325f);
            }
        Gizmos.color = Color.white;
        foreach (Polygon p in polygons)
        {
            for (int i = 0; i < p.points.Count; i++)
            {
                if (i == p.points.Count - 1)
                {
                    Gizmos.DrawLine(p.points[0],p.points[i]);
                }
                else 
                {
                    Gizmos.DrawLine(p.points[i], p.points[i + 1]);
                }
            }
            if (p.points.Count == 4 && fillDraw)
            {
                Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                Vector3 start = p.points[0]; float increment = Vector3.Distance(p.points[0], p.points[1]) / 100;
                for (int i = 0; i < 100; i++)
                {
                    Gizmos.DrawLine(start, p.points[2]);
                    start = Vector3.MoveTowards(start, p.points[1], increment);
                }
                start = p.points[2]; increment = Vector3.Distance(p.points[2], p.points[3]) / 100;
                for (int i = 0; i < 100; i++)
                {
                    Gizmos.DrawLine(start, p.points[0]);
                    start = Vector3.MoveTowards(start, p.points[3], increment);
                }
                start = p.points[1]; increment = Vector3.Distance(p.points[1], p.points[2]) / 100;
                for (int i = 0; i < 100; i++)
                {
                    Gizmos.DrawLine(start, p.points[3]);
                    start = Vector3.MoveTowards(start, p.points[2], increment);
                }
                start = p.points[3]; increment = Vector3.Distance(p.points[3], p.points[0]) / 100;
                for (int i = 0; i < 100; i++)
                {
                    Gizmos.DrawLine(start, p.points[1]);
                    start = Vector3.MoveTowards(start, p.points[0], increment);
                }
            }
        }
    }
    public List<PathNode> BreadthFirstSearch(PathNode start, PathNode end) {
        //Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        int distanceFromStart = nodes.Count + 1;
        frontier.Enqueue(start, 0);
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                    {
                    youAreHere = n; 
                        if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                        foreach (Node nod in youAreHere.connectedNodes)
                        {
                            if (!visited.Contains(nod.pathNode))
                            {
                                nod.pathNode.parentNode = youAreHere.pathNode;
                            distanceFromStart++;
                                frontier.Enqueue(nod.pathNode, distanceFromStart);
                                visited.Add(nod.pathNode);
                                if (nod.pathNode == end)
                                {
                                    frontier.Clear();
                                    endNode = nod.pathNode;
                                break;
                                }
                            }
                        }
                    }
                
            }
        }
        float cost = 0;
        if(endNode!=null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                cost += endNode.weight;
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        string printout = "Breadth-first in " + visited.Count + " steps, and with cost " + cost + ", found a " + finalPath.Count + "-length path.";
        //foreach(PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
    public List<PathNode> HeuristicSearch(PathNode start, PathNode end)
    {
        //Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); List<PathNode> visited = new List<PathNode>();
        frontier.Enqueue(start, Vector3.Distance(start.pos, end.pos));
        PathNode endNode = null;
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                {
                    youAreHere = n;
                    if (!visited.Contains(youAreHere.pathNode)) visited.Add(youAreHere.pathNode);
                    foreach (Node nod in youAreHere.connectedNodes)
                    {
                        if (!visited.Contains(nod.pathNode))
                        {
                            nod.pathNode.parentNode = youAreHere.pathNode;
                            frontier.Enqueue(nod.pathNode,Vector3.Distance(nod.pathNode.pos, end.pos));
                            visited.Add(nod.pathNode);
                            if (nod.pathNode == end)
                            {
                                frontier.Clear();
                                endNode = nod.pathNode;
                                break;
                            }
                        }
                    }
                }

            }
        }
        float cost = 0;
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                cost += endNode.weight;
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        string printout = "Heuristic in " + visited.Count + " steps, and with cost " + cost + ", found a " + finalPath.Count + "-length path.";
        //foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
    public List<PathNode> DijkstrasSearch(PathNode start, PathNode end)
    {
        //Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); Dictionary<PathNode, float> visited = new Dictionary<PathNode, float>();
        frontier.Enqueue(start, start.weight); List<PathNode> visiTED = new List<PathNode>();
        PathNode endNode = null; float currentWeight;
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                {
                    youAreHere = n;
                    if (!visited.ContainsKey(youAreHere.pathNode)) visited.Add(youAreHere.pathNode, youAreHere.pathNode.weight);
                    foreach (Node nod in youAreHere.connectedNodes)
                    {
                        if (!visited.ContainsKey(nod.pathNode))
                        {
                            frontier.Enqueue(nod.pathNode, nod.pathNode.weight);
                            visited.Add(nod.pathNode, nod.pathNode.weight+youAreHere.pathNode.weight);
                        }
                        currentWeight = visited[nod.pathNode];
                        if (visited.ContainsKey(nod.pathNode)&&currentWeight > nod.pathNode.weight + youAreHere.pathNode.weight)
                        { visited[nod.pathNode] = nod.pathNode.weight + youAreHere.pathNode.weight; 
                        }
                    }
                }
            }
        }
        frontier.Enqueue(start, start.weight);
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                {
                    youAreHere = n;
                    if (!visiTED.Contains(youAreHere.pathNode)) visiTED.Add(youAreHere.pathNode);
                    foreach (Node nod in youAreHere.connectedNodes)
                    {
                        if (!visiTED.Contains(nod.pathNode))
                        {
                            nod.pathNode.parentNode = youAreHere.pathNode;
                            frontier.Enqueue(nod.pathNode, visited[nod.pathNode]);
                            
                            visiTED.Add(nod.pathNode);
                            if (nod.pathNode == end)
                            {
                                frontier.Clear();
                                endNode = nod.pathNode;
                                break;
                            }
                        }
                    }
                }

            }
        }
        float cost = 0;
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                cost += endNode.weight;
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        int steps = visited.Count + visiTED.Count;
        string printout = "Dijkstra's in " + steps + " steps, and with cost " + cost + ", found a " + finalPath.Count + "-length path.";
        //foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
    public List<PathNode> AStarSearch(PathNode start, PathNode end)
    {
        //Debug.Log("Finding path between " + start.gameObject.name + " and " + end.gameObject.name);
        PriorityQueue<PathNode> frontier = new PriorityQueue<PathNode>(); List<PathNode> finalPath = new List<PathNode>(); Dictionary<PathNode, float> visited = new Dictionary<PathNode, float>();
        frontier.Enqueue(start, start.weight); List<PathNode> visiTED = new List<PathNode>();
        PathNode endNode = null; float currentWeight;
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                {
                    youAreHere = n;
                    if (!visited.ContainsKey(youAreHere.pathNode)) visited.Add(youAreHere.pathNode, youAreHere.pathNode.weight);
                    foreach (Node nod in youAreHere.connectedNodes)
                    {
                        if (!visited.ContainsKey(nod.pathNode))
                        {
                            frontier.Enqueue(nod.pathNode, nod.pathNode.weight);
                            visited.Add(nod.pathNode, nod.pathNode.weight + youAreHere.pathNode.weight);
                        }
                        currentWeight = visited[nod.pathNode];
                        if (visited.ContainsKey(nod.pathNode) && currentWeight > nod.pathNode.weight + youAreHere.pathNode.weight)
                        {
                            visited[nod.pathNode] = nod.pathNode.weight + youAreHere.pathNode.weight;
                        }
                    }
                }
            }
        }
        float dis = Vector3.Distance(start.pos, end.pos);
        frontier.Enqueue(start, start.weight+1);
        while (frontier.Count > 0)
        {
            PathNode tempP = frontier.Dequeue();
            foreach (Node n in nodes)
            {
                if (n.pathNode != null && n.pathNode.gameObject.name == tempP.gameObject.name)
                {
                    youAreHere = n;
                    if (!visiTED.Contains(youAreHere.pathNode)) visiTED.Add(youAreHere.pathNode);
                    foreach (Node nod in youAreHere.connectedNodes)
                    {
                        if (!visiTED.Contains(nod.pathNode))
                        {
                            nod.pathNode.parentNode = youAreHere.pathNode;
                            frontier.Enqueue(nod.pathNode, visited[nod.pathNode]+(Vector3.Distance(nod.pathNode.pos, end.pos)/dis));

                            visiTED.Add(nod.pathNode);
                            if (nod.pathNode == end)
                            {
                                frontier.Clear();
                                endNode = nod.pathNode;
                                break;
                            }
                        }
                    }
                }

            }
        }
        float cost = 0;
        if (endNode != null)
            while (endNode != start)
            {
                finalPath.Add(endNode);
                cost += endNode.weight;
                endNode = endNode.parentNode;
            }
        finalPath.Add(endNode);
        finalPath.Reverse();
        int steps = visited.Count + visiTED.Count;
        string printout = "A* in " + steps + " steps, and with cost " + cost + ", found a " + finalPath.Count + "-length path.";
        //foreach (PathNode p in finalPath) { printout += (p.gameObject.name + " "); }
        Debug.Log(printout);
        return finalPath;
    }
}
