using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon : MonoBehaviour
{
    public List<Vector3> points;
    // Start is called before the first frame update
    void Start()
    {
        points.Add(new Vector3(transform.position.x+(transform.lossyScale.x*5), transform.position.y+0.2f, transform.position.z+(transform.lossyScale.z*5)));
        points.Add(new Vector3(transform.position.x - (transform.lossyScale.x * 5), transform.position.y + 0.2f, transform.position.z + (transform.lossyScale.z * 5)));

        points.Add(new Vector3(transform.position.x - (transform.lossyScale.x * 5), transform.position.y + 0.2f, transform.position.z - (transform.lossyScale.z * 5)));
        points.Add(new Vector3(transform.position.x + (transform.lossyScale.x * 5), transform.position.y + 0.2f, transform.position.z - (transform.lossyScale.z * 5)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
