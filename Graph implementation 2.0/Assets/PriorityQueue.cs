using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriorityQueue<DataType> : MonoBehaviour
{
    // An individual element in our Priority Queue
    class PQElement
    {
        public DataType data;
        public float priority;

    }

    // Using a linked list to store all of our data internally
    LinkedList<PQElement> linkedList = new LinkedList<PQElement>();

    public int Count => linkedList.Count;
    // Add a new value into the priority queue, with the given priority
    public void Enqueue(DataType aData, float aPriority) {
        PQElement element = new PQElement();
        element.data = aData; element.priority = aPriority;

        if (linkedList.Count <= 0) linkedList.AddFirst(element);
        else {
            LinkedListNode<PQElement> node =linkedList.First;
            for (int i = 0; i < linkedList.Count; i++)
            {
                if (element.priority <= node.Value.priority) {
                    linkedList.AddBefore(node, element); return;
                }
                node = node.Next;
            }
            linkedList.AddLast(element);
        }
    }
    // Return the element with the lowest priority
    public DataType Dequeue() {
        PQElement firstElement = linkedList.First.Value;
        linkedList.RemoveFirst();
        return firstElement.data;
    }

    public void Clear()
    {
        linkedList.Clear();
    }
}
