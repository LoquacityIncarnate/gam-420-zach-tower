using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public enum AIPaths
    { 
    BreadthFirst,
    Heuristic,
    Dijkstras,
    AStar,
    }
    public AIPaths myPath;
    public List<PathNode> route;
    public int progress;
    [SerializeField] Vector3 target;
    [SerializeField] Graph graph;
    public PathNode currentNode;
    // Start is called before the first frame update
    void Start()
    {
        float distance = Vector3.SqrMagnitude(graph.nodes[0].pathNode.pos - transform.position); Graph.Node closest = graph.nodes[0];
        foreach (Graph.Node n in graph.nodes)
        {
            if (Vector3.SqrMagnitude(n.pathNode.pos - transform.position) < distance)
            {
                closest = n;
                distance = Vector3.SqrMagnitude(n.pathNode.pos - transform.position);
            }
        }
        target = closest.pathNode.pos; currentNode = closest.pathNode;
    }

    // Update is called once per frame
    void Update()
    {
        if (progress < route.Count)
        {
            if (Vector3.SqrMagnitude(transform.position - target) > 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, graph.navSpeed * Time.deltaTime);
                currentNode = route[progress];
            }
            else { progress++;
                if (progress < route.Count) target = route[progress].pos;
            }
        }
    }
}
