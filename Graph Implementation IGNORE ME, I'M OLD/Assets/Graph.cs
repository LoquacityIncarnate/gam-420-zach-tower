using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Graph : MonoBehaviour
{
    public LinkedList<Node> nodeList = new LinkedList<Node>();
    // This class represents a node in our graph
    public class Node
    {
        public GameObject obj = GameObject.Find("Empty");
        
        LinkedList<Node> nodeList = new LinkedList<Node>();
        public PathNode pathNode;
        public List<Node> connectedNodes;
        public void AddNode(PathNode aPathNode)
        {
            Node node = new Node();
            pathNode = aPathNode;
            nodeList.AddLast(node);
            for (int i = 0; i<nodeList.Count;i++)
            {
                LinkedList<Node> countingList = new LinkedList<Node>();
                LinkedList<Node> countingList2 = new LinkedList<Node>();
                countingList = nodeList;
                for (int n = i; n > 0; n--)
                {
                    Node currentNode = countingList.First.Value;
                    countingList.RemoveFirst();
                }
                for (int n = 0; n < nodeList.Count; n++)
                {
                    if ((0<(n-i)&&(n-i)<3)||( 0 > (n - i) && (n - i) > -3))
                    {
                        countingList2 = nodeList;
                        Node currentNode2 = null;
                        for (int k = i; k > 0; k--)
                        {
                            currentNode2 = countingList2.First.Value;
                            countingList2.RemoveFirst();
                        }
                        node.connectedNodes.Add(currentNode2); }
                }
            }
        }
    }
    
    // Add a new node to the graph
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        foreach (Node node in nodeList)
        {
            Gizmos.DrawSphere(node.obj.transform.position, 2);
            foreach (Node n in node.connectedNodes)
            {
                Gizmos.DrawLine(node.obj.transform.position, n.obj.transform.position);
            }
        }
    }
}
