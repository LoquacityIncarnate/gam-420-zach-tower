using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    Graph.Node node;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Awake()
    {
        Graph.Node node= new Graph.Node();
        node.AddNode(this);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(this.gameObject.transform.position, 0.5f);
        foreach(Graph.Node n in node.connectedNodes)
        {
            Gizmos.DrawLine(this.gameObject.transform.position, n.obj.transform.position);
        }
        
    }
}
