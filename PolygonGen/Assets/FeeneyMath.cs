using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FeeneyMath
{
    //This script contains code written by Professor Aria Feeney and distributed to students for use in GAM-420 (AI)
    public static bool Intersect(Vector3 a, Vector3 b, List<List<Vector3>> points)
    {

        for (int poly = 0; poly < points.Count; poly++)
        {
            for (int point = 0; point < points[poly].Count; point++)
            {
                int next = point + 1;//default
                if (point == points[poly].Count - 1)//if end of array
                    next = 0;//i+1 should be equal to the start of the array

                //checks if both lines have
                //a point from the other one on
                //both the left and the right of them
                //(tells you if they intersect)
                if (
                    (CrossProduct(a, b, points[poly][point]) * CrossProduct(a, b, points[poly][next])) < 0 &&
                    (CrossProduct(points[poly][point], points[poly][next], a) * CrossProduct(points[poly][point], points[poly][next], b)) < 0)
                    return true;//did intersect
            }
        }
        return false;//did not intersect
    }

    //https://algs4.cs.princeton.edu/91primitives/
    //https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
    public static float CrossProduct(Vector3 a, Vector3 b, Vector3 c)
    {
        return (b.x - a.x) * (c.z - a.z) - (c.x - a.x) * (b.z - a.z);
    }

}
