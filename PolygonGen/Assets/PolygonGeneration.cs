using ClipperLib;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class Vertex
{
    public Vector3 center;
    public Vertex prev;
    public Vertex next;
    public bool reflex;
}
public class PolygonGeneration : MonoBehaviour
{
    List<PathPolygon> pathPolygons = new List<PathPolygon>();
    List<List<IntPoint>> initialPoints = new List<List<IntPoint>>();
    List<List<Vector3>> endPoints = new List<List<Vector3>>();
    [SerializeField] List<Triangle> triangles = new List<Triangle>();
    Clipper clipper = new Clipper();
    List<Vertex> vertices;
    List<Vertex> ears = new List<Vertex>(); 
    [SerializeField] List<Vector3> initialEars = new List<Vector3>(); [SerializeField] List<Vector3> initialNotEars = new List<Vector3>();
    const int mill = 1000000;
    public void AddPolygon(PathPolygon aPoly)
    {
        List<IntPoint> temp = new List<IntPoint>();
        foreach (var item in aPoly.corners)
        {
            temp.Add(new IntPoint(item.x * mill, item.z * mill));
        }
        initialPoints.Add(temp);
    }
    private void Start()
    {
        // Find the intial polygons we're going to use clipper for
        pathPolygons = GameObject.FindObjectsOfType<PathPolygon>().ToList();
        // Calculate the inital points from those polygons
        foreach (PathPolygon pathPolygon in pathPolygons)
        {
            AddPolygon(pathPolygon);
        }

        // Use clipper to fuse our polygons into one
        clipper.AddPaths(initialPoints, PolyType.ptSubject, true);
        clipper.AddPaths(initialPoints, PolyType.ptClip, true);
        initialPoints.Clear();
        clipper.Execute(ClipType.ctUnion, initialPoints, PolyFillType.pftNonZero);//this sets the value of init points

        // Convert the new points into vector3s now that we're done with clipperlib
        foreach (var poly in initialPoints)
        {
            List<Vector3> newPoly = new List<Vector3>();
            foreach (var point in poly)
            {
                newPoly.Add(new Vector3(point.X, 0, point.Y) / mill);
            }
            endPoints.Add(newPoly);
        }

        foreach (List<Vector3> shape in endPoints)
        { 
            
            if (shape.Count == 3)
            {
                Triangle tempT = new Triangle(); tempT.points[0] = shape[0]; tempT.points[1] = shape[1]; tempT.points[2] = shape[2];
                break;
            }
            vertices = new List<Vertex>();
            for (int i = 0; i < shape.Count; i++)
            {
                Vertex v = new Vertex();
                v.center = shape[i];
                vertices.Add(v);
            }
            for (int i = 0; i < vertices.Count; i++)
            {
                if (i == 0) vertices[i].prev = vertices[vertices.Count - 1];
                else vertices[i].prev = vertices[i - 1];
                if (i == vertices.Count - 1) vertices[i].next = vertices[0];
                else vertices[i].next = vertices[i + 1];
            }
            for (int i = 0; i < vertices.Count; i++)
            {
                ReflexOrConvex(vertices[i]);
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                if (checkIfEar(vertices[i]))
                {
                    ears.Add(vertices[i]); 
                }
            }

            while(true)
            {
                //This means we have just one triangle left
                if (vertices.Count == 3)
                {
                    //The final triangle
                    Triangle tempT = new Triangle();
                    tempT.points[0] = vertices[0].center; tempT.points[1] = vertices[1].center; tempT.points[2] = vertices[2].center;
                    triangles.Add(tempT);
                    break;
                }

                //Make a triangle of the first ear
                Vertex earVertex = ears[0];

                Vertex earVertexPrev = earVertex.prev;
                Vertex earVertexNext = earVertex.next;

                Triangle newTriangle = new Triangle();
                newTriangle.points[0] = earVertex.center;
                newTriangle.points[0] = earVertexPrev.center;
                newTriangle.points[0] = earVertexNext.center;
                triangles.Add(newTriangle);

                //Remove the vertex from the lists
                ears.Remove(earVertex);

                vertices.Remove(earVertex);
                //Update the previous vertex and next vertex
                earVertexPrev.next = earVertexNext;
                earVertexNext.prev = earVertexPrev;

                //...see if we have found a new ear by investigating the two vertices that was part of the ear
                ReflexOrConvex(earVertexPrev);
                ReflexOrConvex(earVertexNext);

                ears.Remove(earVertexPrev);
                ears.Remove(earVertexNext);

                if(checkIfEar(earVertexPrev)) ears.Add(earVertexPrev);
                if (checkIfEar(earVertexNext)) ears.Add(earVertexNext);
            }
            /*ears = new List<Triangle>(); notEars = new List<Triangle>();
            while (shape.Count >= 3)
            {
                if (shape.Count == 3)
                {
                    Triangle tempT = new Triangle();
                    for (int i = 0; i < 3; i++)
                        tempT.points[i] = shape[i];
                    triangles.Add(tempT); break;
                }
                for (int i = 0; i < shape.Count; i++)
                {
                    Triangle t = new Triangle();
                    if (i == 0)
                    {
                    t.points[0] = shape[shape.Count - 1]; t.points[1] = shape[i]; t.points[2] = shape[i + 1];
                    checkIfEar(t);
                    }
                    else if (i == shape.Count - 1)
                    {
                    t.points[0] = shape[i - 1]; t.points[1] = shape[i]; t.points[2] = shape[0];
                    checkIfEar(t);
                    }
                    else
                    {
                    t.points[0] = shape[i - 1]; t.points[1] = shape[i]; t.points[2] = shape[i + 1];
                    checkIfEar(t);
                    }
                }
            Debug.Log(ears.Count + " ears and " + notEars.Count + " non-ears, " + shape.Count + " points remaining.");
            
                    Vector3 earVec = ears[0].points[1]; Triangle prev = new Triangle(); Triangle next = new Triangle();
                bool match0 = false; bool match2 = false;
                    for (int i = 0; i < perimeter.Count; i++)
                    {
                    if (perimeter[i].points[1] == ears[0].points[0]) { prev = perimeter[i]; match0=true; }
                    if (perimeter[i].points[1] == ears[0].points[2]) { next = perimeter[i]; match2=true; }
                    }
                if (match0 && match2)
                {
                    triangles.Add(ears[0]);
                    ears.Remove(ears[0]);
                    shape.Remove(earVec);
                    ears.Remove(prev); ears.Remove(next);
                    if (prev != null) checkIfEar(prev); if (next != null) checkIfEar(next);
                }
                else
                {
                    if (!match0) { Debug.Log("Error! Point " + ears[0].points[0] + " not found for the triangle at point " + ears[0].points[1] + "!"); break; }
                    if (!match2) { Debug.Log("Error! Point " + ears[0].points[2] + " not found for the triangle at point " + ears[0].points[1] + "!"); break; }

                }
            } */
        }
        Debug.Log(triangles.Count);
    }
    private void ReflexOrConvex(Vertex v)
    {
        v.reflex = false;
        if (FeeneyMath.CrossProduct(v.prev.center, v.center, v.next.center) <= 0)
            v.reflex = true;
    }
    private bool checkIfEar(Vertex v)
    {
        if (v.reflex) { initialNotEars.Add(v.center); return false; }
        /*for (int i = 0; i < vertices.Count; i++)
        {
            //We only need to check if a reflex vertex is inside of the triangle
            if (vertices[i].reflex)
            {
                //This means inside and not on the hull
                if (FeeneyMath.Intersect())
                {
                    hasPointInside = true;

                    break;
                }
            }
        }*/
        /*Vector2 prev = new Vector2(t.points[1].x - t.points[0].x, t.points[1].z - t.points[0].z);
        Vector2 next = new Vector2(t.points[2].x - t.points[1].x, t.points[2].z - t.points[1].z);
        if ((prev.x * next.y) - (prev.y * next.x) <= 0f)
        {
            if (!notEars.Contains(t) && !initialNotEars.Contains(t.points[1]))
            {
                
                notEars.Add(t);
                initialNotEars.Add(t.points[1]);
            }
            perimeter.Add(t);
            return;
        }
        else if (!ears.Contains(t) && !initialEars.Contains(t.points[1]))
        {
                ears.Add(t);
                initialEars.Add(t.points[1]);
        }
        perimeter.Add(t);*/
        if(!initialEars.Contains(v.center))
        initialEars.Add(v.center);
        return true;
    }
    private void OnDrawGizmos()
    {
        if (!UnityEditor.EditorApplication.isPlaying)
        {
            return;
        }
        foreach (var item in endPoints)
        {
            for (int i = 1; i <= item.Count; i++)
            {
                if (i != item.Count)
                {
                    Gizmos.DrawLine(item[i], item[i - 1]);
                }
                else
                {
                    Gizmos.DrawLine(item[0], item[i - 1]);
                }
            }
        }
        Gizmos.color = Color.red;
        foreach (Triangle t in triangles)
        {
            Gizmos.DrawLine(t.points[0], t.points[1]); Gizmos.DrawLine(t.points[2], t.points[1]); Gizmos.DrawLine(t.points[0], t.points[2]);
        }
        Gizmos.color = Color.blue;
        foreach (Vector3 v in initialEars)
        {
            Gizmos.DrawSphere(v, 1f);
        }
        Gizmos.color = Color.cyan;
        foreach (Vector3 v in initialNotEars)
        {
            Gizmos.DrawSphere(v, 1f);
        }
        /*Gizmos.color = Color.magenta;
        for(int i = 0; i<notEars.Count; i++)
        {
            Vector3 a = new Vector3(notEars[i].points[0].x, notEars[i].points[0].y+(i), notEars[i].points[0].z);
            Vector3 b = new Vector3(notEars[i].points[1].x, notEars[i].points[1].y + (i), notEars[i].points[1].z);
            Vector3 c = new Vector3(notEars[i].points[2].x, notEars[i].points[2].y + (i), notEars[i].points[2].z);
            Gizmos.DrawLine(a, b); Gizmos.DrawLine(b, c); Gizmos.DrawLine(a, c);
        }*/
    }
}

