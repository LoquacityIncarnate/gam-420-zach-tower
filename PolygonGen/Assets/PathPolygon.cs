using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPolygon : MonoBehaviour
{
    [SerializeField] public int weight;
    [HideInInspector] public float width;
    [HideInInspector] public float height;
    [HideInInspector] public List<Vector3> corners = new List<Vector3>();

    private void Awake()
    {
        width = transform.lossyScale.x * 10;
        height = transform.lossyScale.z * 10;
        corners.Add(new Vector3(transform.position.x - width / 2, 0, transform.position.z + height / 2));
        corners.Add(new Vector3(transform.position.x + width / 2, 0, transform.position.z + height / 2));
        corners.Add(new Vector3(transform.position.x + width / 2, 0, transform.position.z - height / 2));
        corners.Add(new Vector3(transform.position.x - width / 2, 0, transform.position.z - height / 2));

    }
}

public class Triangle
{
    public Vector3[] points = new Vector3[3];
}